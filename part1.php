<?php

class OVNI {
  protected $cometas = array();

  public function __construct(array $cometas, $divisao = 45){
    if(is_array($cometas)) {
      $this->cometas = $cometas;
    }
    $this->divisao = $divisao;
  }

  public function NaoVai(){
    $excluidos = array();

    foreach ($this->cometas as $cometa) {
      $nome = $this->esquema_engenhoso($cometa['cometa']);
      $grupo = $this->esquema_engenhoso($cometa['grupo']);

      if ($nome != $grupo) {
        $excluidos[] = $cometa['grupo'];
      }
    }

    return $excluidos;
  }

  protected function esquema_engenhoso($cometa){
    if (empty($cometa) || !preg_match('/[A-Za-z]+/', $cometa) || !is_numeric($this->divisao)) {
      return 0;
    }

    $multiplicacao = 1;

    for ($i=0; $i<strlen($cometa);$i++) {
      $multiplicacao *= (ord($cometa[$i]) - ord('A') + 1);
    }

    return $multiplicacao % $this->divisao;
  }
}

$grupos = array(
  array('cometa'=>'HALLEY', 'grupo'=>'AMERELO'),
  array('cometa'=>'ENCKE', 'grupo'=>'VERMELHO'),
  array('cometa'=>'WOLF', 'grupo'=>'PRETO'),
  array('cometa'=>'KUSHIDA', 'grupo'=>'AZUL'),
);

$ovni = new OVNI($grupos);
// $ovni = new OVNI($grupos, 55);

echo json_encode($ovni->NaoVai());