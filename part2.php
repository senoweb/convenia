<?php
date_default_timezone_set('America/Sao_Paulo');

require 'lib/Carbon/Carbon.php';

use Carbon\Carbon;

Class Clt {
  public function __construct(array $funcionarios){
    if(is_array($funcionarios)) {
      $this->funcionarios = $funcionarios;
    }
  }

  public function ferias(){
    $funcionarios = Array();

    foreach($this->funcionarios as $funcionario){
      $funcionarios[] = $this->calcula_ferias($funcionario);
    }

    return $funcionarios;
  }

  private function calcula_ferias($funcionario){
    Carbon::setToStringFormat('d/m/Y');

    $admitido = Carbon::createFromFormat('d/m/Y', $funcionario['data']);
    $aquisitivo = Carbon::parse($admitido->toDateTimeString())->addYear()->subDay();
    $concessivo = Carbon::parse($aquisitivo->toDateTimeString())->addYear();
    $ferias = ($aquisitivo->isPast() ? true : false);

    $periodo = 30;
    $faltas  = 0;

    if(isset($funcionario['faltas'])){
      $faltas = $funcionario['faltas'];
      if($faltas >= 6 && $faltas <= 14) $periodo = 24;
      if($faltas >= 15 && $faltas <= 23) $periodo = 18;
      if($faltas >= 24 && $faltas <= 32) $periodo = 12;
      if($faltas > 32) $periodo = 0;
    }

    $return  = Array(
      'faltas' => $faltas,
      'data' => $admitido->format('d/m/Y'),
      'aquisitivo' => $aquisitivo->format('d/m/Y'),
      'concessivo' => $concessivo->format('d/m/Y'),
      'periodo' => $periodo,
      'ferias' => $ferias,
    );

    return array_merge($funcionario, $return);
  }
}

$funcionarios = array(
  array('name'=>'Func 1', 'data' => '01/01/2014'),
  array('name'=>'Func 2', 'data' => '11/07/2013', 'faltas' => 13),
  array('name'=>'Func 3', 'data' => '15/06/2014', 'faltas' => 20),
  array('name'=>'Func 4', 'data' => '21/07/2015', 'faltas' => 28),
  array('name'=>'Func 5', 'data' => '01/02/2015', 'faltas' => 35),
);

$clt = new Clt($funcionarios);
$ferias = $clt->ferias();

if(isset($_GET['type']) && strtolower($_GET['type']) == 'json'){
  echo json_encode($ferias);
} else {
  echo '<table border=1>';
  echo '<header>';
  echo '<th>Nome</th>';
  echo '<th>Admitido</th>';
  echo '<th>Periodo Aquisitivo</th>';
  echo '<th>Periodo Concessivo</th>';
  echo '<th>Periodo de ferias</th>';
  echo '<th>Numero de faltas</th>';
  echo '<th>Elegivel a ferias</th>';
  echo '</header>';
  echo '<body>';
  foreach($ferias as $funcionario){
    echo '<tr>';
    echo '<td>' .$funcionario['name']. '</td>';
    echo '<td>' .$funcionario['data']. '</td>';
    echo '<td>' .$funcionario['aquisitivo']. '</td>';
    echo '<td>' .$funcionario['concessivo']. '</td>';
    echo '<td>' .$funcionario['periodo']. '</td>';
    echo '<td>' .$funcionario['faltas']. '</td>';
    echo '<td>' .($funcionario['ferias'] ? 'Sim' : 'Nao'). '</td>';
    echo '</tr>';
  }
  echo '</body>';
  echo '</table>';
}